output "vpc_details" {
  value = {
    vpc_id = aws_vpc.vpc.id
    vpc_arn = aws_vpc.vpc.arn
    igw_id = var.igw == true ? aws_internet_gateway.igw[0].id : null
  }
}